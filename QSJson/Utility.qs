
Script GenerateCompact
(s, r)

U4 loop, n;

switch (GetActualType(r))
{
	case "IMap"
		StrAppend(s, "{");
		n = GetCount(r);
		loop = 0;
		while (loop < n)
		{
			StrAppend(s,"\"", IMapGetKey(r, loop), "\":");
			if (AppendValueCompact(s, r[loop]))
			{
				return 1;
			}
			if (loop + 1 <> n)		
			{
				StrAppend(s, ",");
			}
			Inc(loop);
		}
		StrAppend(s, "}");
		return 0;
		
	case "Vector"
		StrAppend(s, "[");
		n = GetCount(r);
		loop = 0;
		while (loop < n)
		{
			if (AppendValueCompact(s, r[loop]))
			{
				return 1;
			}
			if (loop + 1 <> n)
			
			{
				StrAppend(s, ",");
			}
			Inc(loop);
		}
		StrAppend(s, "]");
		return 0;
		
	default
		return 1;
}
EndScript

Script Generate
(s, r)

U4 loop, n;

switch (GetActualType(r))
{
	case "IMap"
		StrAppend(s, StrRepeat("  ", StackCount()), "{\r\n");
		n = GetCount(r);
		loop = 0;
		while (loop < n)
		{
			StrAppend(s, StrRepeat("  ", StackCount()), "\"", IMapGetKey(r, loop), "\" : ");
			if (AppendValue(s, r[loop]))
			{
				return 1;
			}
			if (loop + 1 == n)
			{
				StrAppend(s, "\r\n");
			}
			else
			{
				StrAppend(s, ",\r\n");
			}
			Inc(loop);
		}
		StrAppend(s, StrRepeat("  ", StackCount()), "}");
		return 0;
		
	case "Vector"
		StrAppend(s, StrRepeat("  ", StackCount()), "[\r\n");
		n = GetCount(r);
		loop = 0;
		while (loop < n)
		{
			if (AppendValue(s, r[loop]))
			{
				return 1;
			}
			if (loop + 1 == n)
			{
				StrAppend(s, "\r\n");
			}
			else
			{
				StrAppend(s, ",\r\n");
			}
			Inc(loop);
		}
		StrAppend(s, StrRepeat("  ", StackCount()), "]");
		return 0;
		
	default
		return 1;
}
EndScript

Script ShowContainer
(x)

U0 n, loop;

n = GetCount(x);
loop = 0;
switch (GetActualType(x))
{
	case "Vector"
	case "PVector"
		while (loop < n)
		{
			switch (GetActualType(x[loop]))
			{
			case "Vector"
			case "PVector"
			case "IMap"
			case "PIMap"
				Print (StrRepeat("  ", StackCount()), loop, ") - ", GetActualType(x[loop]), " ", GetCount(x[loop]));
				ShowContainer(x[loop]);
				break;
			
			default
				switch (GetActualType(x[loop]))
				{
				case "Boolean"
				case "U0"
					Print (StrRepeat("  ", StackCount()), loop, ") ", x[loop], " ", GetActualType(x[loop]));
					break;
					
				default
					Print (StrRepeat("  ", StackCount()), loop, ") ", x[loop]);
					break;
				}
				break;
			}
			Inc(loop);
			
		}
		break;
		
	case "IMap"
	case "PIMap"
		while (loop < n)
		{
			switch (GetActualType(x[loop]))
			{
			case "Vector"
			case "PVector"
			case "IMap"
			case "PIMap"
				Print (StrRepeat("  ", StackCount()), loop, ") ", IMapGetKey(x, loop), " - ", GetActualType(x[loop]), " ", GetCount(x[loop]));
				ShowContainer(x[loop]);
				break;
			
			default
				switch (GetActualType(x[loop]))
				{
				case "Boolean"
				case "U0"
					Print (StrRepeat("  ", StackCount()), loop, ") ", IMapGetKey(x, loop), " ", x[loop], " ", GetActualType(x[loop]));
					break;
					
				default
					Print (StrRepeat("  ", StackCount()), loop, ") ", IMapGetKey(x, loop), " ", x[loop]);
					break;
				}
				break;
			}
			Inc(loop);
		}
		break;
		
	case "Map"
	case "PMap"
		break;
		
	default
		Print (GetActualType(x), " not displayed");
}

EndScript


Script AppendValue
(s, r)
switch (GetActualType(r))
{
	case "IMap"
		return Generate(s, r);
		
	case "Vector"
		return Generate(StrAppend(s, "\r\n"), r);
		
	case "String"
		StrAppend(s, "\"", r, "\"");
		return 0;
	
	case "U0"
		StrAppend(s, "null");
		return 0;
		
	case "Boolean"
		if (r)
		{
			StrAppend(s, "true");
		}
		else
		{
			StrAppend(s, "false");
		}
		return 0;
		
	case "I8"
		StrAppend(s, r);
		return 0;
		
	case "F8"
		StrAppend(s, r);
		return 0;
		
	default
		return 1;
}
EndScript

Script AppendValueCompact
(s, r)
switch (GetActualType(r))
{
	case "IMap"
		return GenerateCompact(s, r);
		
	case "Vector"
		return GenerateCompact(StrAppend(s, ""), r);
		
	case "String"
		StrAppend(s, "\"", r, "\"");
		return 0;
	
	case "U0"
		StrAppend(s, "null");
		return 0;
		
	case "Boolean"
		if (r)
		{
			StrAppend(s, "true");
		}
		else
		{
			StrAppend(s, "false");
		}
		return 0;
		
	case "I8"
		StrAppend(s, r);
		return 0;
		
	case "F8"
		StrAppend(s, r);
		return 0;
		
	default
		return 1;
}
EndScript